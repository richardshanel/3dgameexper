package engine.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileUtils {
    public static String loadAsString(String path) throws FileNotFoundException {
        StringBuilder res = new StringBuilder();
        Scanner scanner = new Scanner(new File(path));
        while (scanner.hasNext())
            res.append(scanner.next()).append("\n");
        return res.toString();
    }
}
