package engine.graphics;
import engine.utils.FileUtils;
import org.lwjgl.opengl.GL40;
import java.io.FileNotFoundException;

public class Shader {
    private int programID;

    public Shader(String vertexPath, String fragmentPath) throws FileNotFoundException {
        String vertexFile = FileUtils.loadAsString(vertexPath);
        String fragmentFile = FileUtils.loadAsString(fragmentPath);
        programID = GL40.glCreateProgram();
        int vertexID = GL40.glCreateShader(GL40.GL_VERTEX_SHADER);
        int fragmentID = GL40.glCreateShader(GL40.GL_FRAGMENT_SHADER);
        GL40.glShaderSource(vertexID, vertexFile);
        GL40.glCompileShader(vertexID);
        GL40.glShaderSource(fragmentID, fragmentFile);
        GL40.glAttachShader(programID, vertexID);
        GL40.glAttachShader(programID, fragmentID);
        GL40.glLinkProgram(programID);
        GL40.glValidateProgram(programID);
        GL40.glDeleteShader(vertexID);
        GL40.glDeleteShader(fragmentID);
    }

    public void bind() {
        GL40.glUseProgram(programID);
    }

    public void unbind() {
        GL40.glUseProgram(0);
    }

    public void destroy() {
        GL40.glDeleteProgram(programID);
    }
}
