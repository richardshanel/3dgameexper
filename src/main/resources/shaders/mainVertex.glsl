void main() {
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
    gl_Color = gl_Position + 0.5;
    gl_FrontColor = gl_Color;
}
